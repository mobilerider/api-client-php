<?php
/**
 * Copyright 2012 MobileRider Networks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

if (!function_exists('curl_init')) {
  throw new Exception('The CURL PHP extension is required.');
}
if (!function_exists('json_decode')) {
  throw new Exception('The JSON PHP extension is required.');
}

/**
 * Provides the methods to access content on the Mobilerider service.
 */
class Mobilerider
{
  /**
   * Version of the class.
   */
  const version = '0.1.1';

  /**
   * Base url of the API service.
   */
  public static $API_URL = 'http://api.mobilerider.com/v3/';

  /**
   * Default options for cUrl.
   */
  public static $CURL_OPTS = array(CURLOPT_RETURNTRANSFER => true, 
                                   CURLOPT_TIMEOUT => 60,
                                   CURLOPT_USERAGENT => 'mobilerider-api-client-0.1.1'
                                   );

  /**
   * Application ID.
   *
   * @type string
   */
  protected $appId;

  /**
   * Application secret token
   *
   * @type string
   */
  protected $appSecret;

  /**
   * Initializes a new instance of the class.
   *
   * @param string $appId The application id.
   * @param string $appSecret The application secret.
   * @constructor
   */
  public function __construct($appId, $appSecret)
  {
    $this->appId = $appId;
    $this->appSecret = $appSecret;
  }

  /**
   * Returns the application id.
   *
   * @return string The application id.
   */
  public function getAppId()
  {
    return $this->appId;
  }

  /**
   * Returns the application secret token.
   *
   * @return string The application secret token.
   */
  public function getAppSecret()
  {
    return $this->appSecret;
  }

  /**
   * Generates a token based on the arguments parameter.
   *
   * @param array $arguments The arguments for the request.
   * @return string The token.
   */ 
	protected function prepareToken($arguments)
	{
		ksort($arguments);
    if(array_key_exists('token', $arguments))
      unset($arguments['token']);
		$rawToken = http_build_query($arguments);
		$hashToken = hash_hmac('sha256', $rawToken, $this->appSecret);
		return $hashToken;
  }

  /**
   * Makes a request with the arguments passed and returns the PHP object
   * resulting from decoding the json response.
   *
   * @param string $model The object model to query (media, channel, etc.)
   * @param string $id Optional id of the element from $model to return.
   * @param array $arguments Optional array of arguments.
   * @return object The result of the request as an object.
   */
  protected function makeRequest($model, $id='', $arguments=array())
  {
    if(is_array($id)) {
      $arguments = $id;
      $id = '';
    }

    $curlOpts = self::$CURL_OPTS;
    if(!array_key_exists('appid', $arguments))
      $arguments['appid'] = $this->appId;
    $token = $this->prepareToken($arguments);
    $arguments['token'] = $token;
    $url = self::$API_URL . $model . '/';
    if($id != '')
      $url .= $id . '/';
    $curlOpts[CURLOPT_URL] = $url . '?' . http_build_query($arguments);

    $curlHandler = curl_init();
    curl_setopt_array($curlHandler, $curlOpts);
    $result = curl_exec($curlHandler);

    if($result === false) {
      $message = 'cUrl error: ' . curl_error($curlHandler);
      $exception = new Exception($message);
      curl_close($curlHandler);
      throw $exception;
    }
    curl_close($curlHandler);

    $resultObject = json_decode($result);
    $message = '';
    if(function_exists('json_last_error')) {
      switch(json_last_error()) {
      case JSON_ERROR_DEPTH:
        $message = 'Maximum stack depth exceeded';
        break;
      case JSON_ERROR_STATE_MISMATCH:
        $message = 'Underflow or the modes mismatch';
        break;
      case JSON_ERROR_CTRL_CHAR:
        $message = 'Unexpected control character found';
        break;
      case JSON_ERROR_SYNTAX:
        $message = 'Syntax error, malformed JSON';
        break;
      case JSON_ERROR_UTF8:
        $message = 'Malformed UTF-8 characters, possibly incorrectly encoded';
        break;
      case JSON_ERROR_NONE:
        break;
      default:
        $message = 'Unknown error.';
        break;
      }
    }
    if($message != '') {
      $message = 'json_decode error: ' . $message . "\n" . $result;
      throw new Exception($message);
    }

    if($resultObject->status != 'ok')
      throw new Exception('API error: ' . $resultObject->error);

    return $resultObject;
  }

  /**
   * Returns an array with media objects that meet the filters from $params.
   *
   * @param array $params Optional array of filters.
   * @return array The array of objects.
   */
  public function getMedia($params=array())
  {
    $result = $this->makeRequest('media', $params);
    return $result->response->items;
  }

  /**
   * Returns the application object related to this application id.
   *
   * @param array $params Optional array of filters.
   * @return object The object.
   */
  public function getApplication($params=array())
  {
    $result = $this->makeRequest('application', $params);
    return $result->response->items[0];
  }

  /**
   * Returns an array with channel objects that meet the filters from $params.
   *
   * @param array $params Optional array of filters.
   * @return array The array of objects.
   */
  public function getChannel($params=array())
  {
    $result = $this->makeRequest('channel', $params);
    return $result->response->items;
  }

}
?>